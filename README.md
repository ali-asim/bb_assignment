

## Technology used
- React js - javascript framework
- Webpack - as the bundler
- Backend - firebase
- Css Compiler - SASS
- Yarn - Dependency Management
- Other libraries - jQuery, Bootstrap, Slick
- Post request calls - Campaign Monitor Api
- Bitbucket for Git repo

## How to 
- Clone project -- `git clone https://ali-asim@bitbucket.org/ali-asim/bb_assignment.git`
- Browse into the project root `cd bb-assignment`
- Install yarn globally -- `npm install -g yarn`
- `yarn install`
- Once the dependencies are installed type `yarn start`
- The site should load up on your default browser under `localhost:3000`
- To build the project type `yarn build`

## Whats under the hood?
- Most of the code resides under `src/components`
- Idea of blocks is, you build them once and resue them on separate pages
- There is additional routing system in place for demonstration purposes, example `/about` or `/notfound`
- I have added **Firebase** integration for demonstration purposes and also included is the json file for reference under config.

## But what about the performace?
- All assets are optimized as well as concatenated for fewer requests
- The site is was tested on with deployed to aws servers
-- As per gtmetrix - the Y-slow score is @ 92% with page speed at 70% (could be improved with different assets and enabling gzip)
-- dummy meta description was placed in, having said that twitter cards, facebook id, sharethis,  analytics and structural schema were considered but were out of scope for this exercise.

## Tools Used
- Create React App
- Visual Studio Code
- Photopshop CC
- Git cli
- Yarn