import React from 'react';

class Content extends React.Component {
  render(){
    const $items = this.props.content;
    return (
      <section id="services" className="__module_content __module_section_spacing">
        <div className="col-md-7 col-margin-auto">
          {
            $items && 
            $items
            .map((item, index) => (
              <div key={index}>
              <span dangerouslySetInnerHTML={{__html: item.content}} />
              </div>
            ))
          }
        </div>
      </section>
    )
  }
}
export default Content;