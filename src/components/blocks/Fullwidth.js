import React from 'react';

class Fullwidth extends React.Component {
  render(){
    const $items = this.props.fullwidth;
    return (
      <section className="__module_fullwidth __module_section_spacing">
      {
        $items && 
        $items
        .map((item, index) => (
          <div key={index} className="row bg-image" style = {{backgroundImage: 'url(' + item.image + ')'}}></div>
        ))
      }
      </section>
    )
  }
}
export default Fullwidth;