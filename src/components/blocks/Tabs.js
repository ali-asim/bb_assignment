import React from 'react';
import jQuery from 'jquery';

class Tabs extends React.Component {
  componentDidMount(){
    setTimeout(function(){ 
      jQuery('.__module_tabs .nav-tabs a:first').trigger("click");
    }, 1700);
  }
  render(){
    const $items = this.props.tabs;
    return (
      <section id="projects" className="__module_tabs __module_section_spacing">
        <div className="container">
          <ul className="nav nav-tabs" role="tablist">
          {
            $items && 
            $items
            .map((item, index) => (
              <li key={index} className="nav-item">
                <a className="nav-link" href={`#tab-${index}`} role="tab" data-toggle="tab">{item.title}</a>
              </li>
            ))
          }
          </ul>
          <div className="tab-content">
          {
            $items && 
            $items
            .map((item, index) => (
              <div  key={index} role="tabpanel" className="tab-pane fade in" id={`tab-${index}`}>
                <div className="row">
                  <div className="col-md-6">
                    <img className="width100" src={item.image} alt={item.heading} />
                  </div>
                  <div className="col-md-6">
                    <h3>{item.heading}</h3>
                    <span dangerouslySetInnerHTML={{__html: item.content}} />
                    <a href={item.linkUrl} className="btn btn-primary">{item.link}</a>
                  </div>
                </div>
              </div>
            ))
          }
          </div>
        </div>
      </section>
    )
  }
}
export default Tabs;