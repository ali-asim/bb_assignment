import React from 'react';

class Landing extends React.Component {
  render(){
    const $items = this.props.landing;
    return (
      <section id="home" className="__module-landing"> 
          {
            $items && 
            $items
            .map((item, index) => (
              <div key={index} role="main" className="inner cover bg-overlay" style = {{backgroundImage: 'url(' + item.background + ')'}}>
                <div>
                  <h1 className="cover-heading">{item.heading}</h1>
                  <p className="lead">{item.copy}</p>
                </div>
              </div>
            ))
          }
      </section>
    )
  }
}
export default Landing;