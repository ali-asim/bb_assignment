import React from 'react';

class Grid extends React.Component {
  render(){ 
    const $items = this.props.grid;
    return (
    <section id="about" className="__module_grid __module_section_spacing">
      <div className="container">
        <div className="row">
          {
            $items && 
            $items
            .map((item, index) => (
            <div className="col-sm-4" key={index}>
              <div className="card">
                <a href={item.linkUrl} className="card-block" style={{backgroundImage: 'url(' + item.background + ')'}}>
                  <div className="card-content">
                    <h3 className="card-title">{item.heading}</h3>
                    <p className="card-text">{item.copy}</p>
                  </div>
                </a>
              </div>
            </div>
            ))
          }
        </div>
      </div>
    </section>
    )
  }
}
export default Grid;