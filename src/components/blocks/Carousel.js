import React from 'react';
import Slider from 'react-slick';
class Carousel extends React.Component {
  render(){
    const $items = this.props.carousel;
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 2,
      slidesToScroll: 2,
      responsive:[
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
      }]
    };
    return (
      <section className="__module_slider __module_section_spacing">
        <div className="container"> 
          <Slider {...settings}>
          {
            $items &&
            $items
            .map((item, index) => (
              <div key={index}>
                <img className="width100" src={item.image} alt={item.title} />
              </div>
            ))
          }
          </Slider>
        </div>
      </section>
    )
  }
}
export default Carousel;
