import React from 'react';
import jQuery from 'jquery';

class Mailinglist extends React.Component {
  componentDidMount(){
    const $el_mlForm = jQuery('#subForm');
    const $el_message = jQuery('.alert-message');
    $el_mlForm.submit(function (e) {
        e.preventDefault();
        jQuery.getJSON(
        this.action + "?callback=?",
        jQuery(this).serialize(),
        function (data) {
          //$el_message.addClass('show');
          $el_message.find('.alert').removeClass('show');
            if (data.Status === 400) {
                $el_message.find('.alert-warning').addClass('show')
                .find('#contact-warning-msg').text(`Error: ${data.Message}`);
            } else { // 200
                $el_mlForm.fadeOut();
                $el_message.find('.alert-success').addClass('show')
                .find('#contact-success-msg').text(`Success: ${data.Message}`);
            }
        });
    });
  }
  render(){
    return (
      <section id="contact" className="__module_mailinglist __module_section_spacing">
        <div className="container">
          <h2>Join Mailing List</h2>
          <form action="https://ghostwriters1.createsend.com/t/j/s/vulltt/" method="post" id="subForm">
            <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <label htmlFor="name">Name:</label>
                  <input type="text" className="form-control" name="cm-name" placeholder="Enter your name" id="name" />
                </div>
              </div>
              <div className="col-md-6">
                <div className="form-group">
                  <label htmlFor="email">Email Address:</label>
                  <input type="email" className="form-control" id="email" name="cm-vulltt-vulltt" placeholder="Enter your email" />
                </div>
              </div>
            </div>
            <button type="submit" className="btn btn-secondary">Submit</button>
          </form>
          <div className="alert-message">
            <hr/>          
            <div className="alert alert-warning alert-dismissible fade" role="alert">
              <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <div id="contact-warning-msg"></div>
            </div>
            <div className="alert alert-success alert-dismissible fade" role="alert">
              <div id="contact-success-msg"></div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}
export default Mailinglist;