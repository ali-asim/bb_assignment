import React from 'react';
import {DB_CONFIG} from '../../config/config';
import firebase from 'firebase/app';
import 'firebase/database';

import Header from '../layout/Header';
import Grid from '../blocks/Grid';
import Content from '../blocks/Content';
import Carousel from '../blocks/Carousel';
import Fullwidth from '../blocks/Fullwidth';
import Tabs from '../blocks/Tabs';
import Mailinglist from '../blocks/Mailinglist';
import Footer from '../layout/Footer';

class About extends React.Component {
  constructor(props){
    super(props);
    this.app = firebase.initializeApp(DB_CONFIG);
    this.database = this.app.database().ref('/');
    this.state = {
      blocks: [],
      loading: true
    }
  }
  componentWillMount(){
    let $blocks = this.state.blocks;
    this.database.once('value', snap => {
      $blocks = snap.val();
      this.setState({
        blocks: $blocks
      });
    });
  }
  componentDidMount(){
    setTimeout(() => this.setState({ loading: false }), 1500); 
  }
  render(){
    const $header = this.state.blocks.header;
    const $carousel = this.state.blocks.carousel;
    const $grid = this.state.blocks.grid;
    const $content = this.state.blocks.content;
    const $fullwidth = this.state.blocks.fullwidth;
    const $tabs = this.state.blocks.tabs;
    return (
      <div>
        <Header header={$header} />
        <Grid grid={$grid} />
        <Content content={$content} />
        <Carousel carousel={$carousel} />
        <Fullwidth fullwidth={$fullwidth} />
        <Tabs tabs={$tabs} />
        <Mailinglist />
        <Footer />
      </div>
    )
  }
}
export default About;