import React from 'react';
import {DB_CONFIG} from '../../config/config';
import firebase from 'firebase/app';
import 'firebase/database';

import MetaTags from 'react-meta-tags';

import Header from '../layout/Header';
import Landing from '../blocks/Landing';
import Grid from '../blocks/Grid';
import Content from '../blocks/Content';
import Carousel from '../blocks/Carousel';
import Fullwidth from '../blocks/Fullwidth';
import Tabs from '../blocks/Tabs';
import Mailinglist from '../blocks/Mailinglist';
import Footer from '../layout/Footer';

class Home extends React.Component {
  constructor(props){
    super(props);
    this.app = firebase.initializeApp(DB_CONFIG);
    this.database = this.app.database().ref('/');
    this.state = {
      blocks: [],
      loading: true
    }
  }
  componentWillMount(){
    let $blocks = this.state.blocks;
    this.database.once('value', snap => {
      $blocks = snap.val();
      this.setState({
        blocks: $blocks
      });
    });
  }
  render(){
    const $header = this.state.blocks.header;
    const $landing = this.state.blocks.landing;
    const $carousel = this.state.blocks.carousel;
    const $grid = this.state.blocks.grid;
    const $content = this.state.blocks.content;
    const $fullwidth = this.state.blocks.fullwidth;
    const $tabs = this.state.blocks.tabs;
    return (
      <div>
        <MetaTags>
          <title>SimpleLogo - We create campaigns that get ROI. </title>
          <meta name="description" content="SimpleLogo is a digital marketing agency that creates campaigns that get ROI." />
          <meta property="og:title" content="SimpleLogo" />
          <meta property="og:image" content="https://s3.ca-central-1.amazonaws.com/elasticbeanstalk-ca-central-1-857987574675/bb-assignment/logo/sample-logo-black.svg" />
          <meta name="robots" content="noindex, nofollow" />
        </MetaTags>

        <Header header={$header} />
        <Landing landing={$landing} />
        <Grid grid={$grid} />
        <Content content={$content} />
        <Carousel carousel={$carousel} />
        <Fullwidth fullwidth={$fullwidth} />
        <Tabs tabs={$tabs} />
        <Mailinglist />
        <Footer />
      </div>
    )
  }
}
export default Home;