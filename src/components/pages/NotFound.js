import React from 'react';

const NotFound = () => {
  return(
    <section id="home" className="__module-landing"> 
        <div  role="main" className="inner cover bg-overlay">
          <div>
            <p className="lead">Not Found</p>
          </div>
        </div>
    </section>
  )
}

export default NotFound;