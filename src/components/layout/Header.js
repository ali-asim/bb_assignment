import React from 'react';
import jQuery from 'jquery';
const $__body = document.querySelector('.body');

class Header extends React.Component {
  constructor(props){
    super(props);
    this.handleScroll = this.handleScroll.bind(this);
  }
  componentDidMount() {
    $__body.addEventListener('scroll', this.handleScroll);
  }
  componentWillUnmount() {
    $__body.removeEventListener('scroll', this.handleScroll);
  }
  handleScroll() {
    let $height = jQuery('.body').scrollTop();
    if($height  > 100) {
      $__body.classList.add('header-active');
    } if($height === 0) {
      $__body.classList.remove('header-active');
    }
  }
  render() {
    const $header = this.props.header;
    return (
      <header className="masthead clearfix" role="banner">
        <div className="container">
          <div className="inner">
            <a href="/" className="masthead-brand">
            {
              $header && <img src={$header.logo} alt={$header.project} />
            }
            </a>
            <nav id="navigation" className="nav nav-masthead">
              {
                $header && $header.nav
                .map((item, index) => (
                  <a href={item.url} className="nav-link" key={index} >{item.name}</a>
                ))
              }
            </nav>
          </div>
        </div>
      </header>
    )
  }
}
export default Header;