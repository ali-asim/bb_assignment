import React, { Component } from 'react';
import { BrowserRouter as Router , Route, Switch} from 'react-router-dom';

import Home from './pages/Home';
import About from './pages/About';
import NotFound from './pages/NotFound';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      blocks: []
    }
  }
  render() {
    return (
    <div>
        <Router>
          <div>
            <Switch>
              <Route exact path="/" component={Home}/>
              <Route exact path="/about" component={About}/>
              <Route component={NotFound}/>
            </Switch>
          </div>
        </Router>
    </div>
    )
  }
}
export default App;