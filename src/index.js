import React from 'react';
import { render } from 'react-dom';
import './styles/app.css'; 

import App from './components/App';
import jQuery from 'jquery';
import Popper from 'popper.js';
global.jQuery = jQuery;
global.jquery = jQuery;
global.$ = jQuery;
window.Popper = Popper;
const Bootstrap = require('bootstrap');

render(<App />, document.querySelector('#root'));